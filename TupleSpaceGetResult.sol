pragma solidity ^0.4.18;

/**
 * A Linda-like tuple space.
 */
contract TupleSpaceGetResult {
    
    enum SuspensiveOps {READ, TAKE}
    
    struct TupleInfo {
        uint multiplicity;
    }

    struct PendingOps {
        PendingOp[] queue;
        uint opsCount;
    }

    struct PendingOp {
        SuspensiveOps operation;
        string template;
        bool alreadySatisfied;
    }

    event DebugQueueUpdated(string tuple, uint oldQueueLength, uint newQueueLength, string op, address peer);

    event DebugSpaceUpdated(string tuple, string op, address peer);
    
    function TupleSpaceGetResult(string _name, bool _debug) public payable {
        owner = msg.sender;
        debug = _debug;
        name = _name;
    }

    function Dispose() public {
        selfdestruct(owner);
    }
    
    // The address (i.e., the identifier) of this tuplespace's owner
    address owner;

    // A field stating whether debug events should be generated or not
    bool debug;

    // A publicly accessible field containing the name of the tuplespace for debugging purposes
    string public name;
    
    /**
     * This is the data structure actually reifying the tuplespace. 
     * It is a map associating each tuple contained within this tuplespace to its corresponding `TupleInfo` record.
     */
    mapping(string => TupleInfo) tupleSpace;

    mapping(address => PendingOps) pendingOperations;
    
    /**
     * API method corresponding to `Linda`'s `out` primitive.
     * It inserts a tuple within the tuplespace.
     * - `tuple`: is the tuple to be inserted
     * - Cost: O(tupleSpace[tuple].length)
     */
    function Write(string tuple) public {
        require(!strEquals(tuple, ""));
        // Let `info` be the `TupleInfo` record associated to `tuple` (defaults to the empty instance where `quantity` is `0` and `queue` is empty) 
        TupleInfo storage info = tupleSpace[tuple];
        // increases the quantity of available copies of `tuple` 
        info.multiplicity += 1;

        DebugSpaceUpdated(tuple, "write", msg.sender);
    }
    
    /**
     * API method corresponding to `Linda`'s `in` primitive.
     * It retrieves a tuple matching `template` within the tuplespace.
     * - `template`: is the template a the tuple to be retrieved
     * - Cost: O(1) [if debug, O(tupleSpace[template].length)] 
     */
    function Take(string template) public {
        TupleInfo storage matching = tupleSpace[template];

        if (matching.multiplicity > 0) {
            pushPendingOperation(template, SuspensiveOps.TAKE, true, msg.sender);
            matching.multiplicity--;
            DebugSpaceUpdated(template, "take", msg.sender);
        } else {
            pushPendingOperation(template, SuspensiveOps.TAKE, false, msg.sender);
        }

        PendingOps storage pendings = pendingOperations[msg.sender];

        DebugQueueUpdated(template, pendings.opsCount - 1, pendings.opsCount, "take", msg.sender);
    }

    function pushPendingOperation(string template, SuspensiveOps op, bool satisfied, address caller) internal {
        PendingOps storage pendings = pendingOperations[caller];
        if (pendings.opsCount == pendings.queue.length) {
            pendings.queue.length++;
        }
        PendingOp storage newOp = pendings.queue[pendings.opsCount];
        newOp.operation = op;
        newOp.template = template;
        newOp.alreadySatisfied = satisfied;
        pendings.opsCount++;
    }

    function strEquals(string x, string y) private pure returns (bool) {
        return keccak256(x) == keccak256(y);
    }

    // function getPendingOperationOnTemplateForAddress(string template, address caller) internal returns(uint) {
    //     PendingOps storage pendings = pendingOperations[caller];
    //     uint index;
    //     for (index = pendings.opsCount - 1; index >= 0; index--) {
    //         PendingOp storage pending = pendings.queue[index];
    //         if (strEquals(template, pending.template)) break;
    //     }
        
    //     return index;
    // }

    function getSatisfablePendingOperationForAddress(address caller) private view returns(int) {
        PendingOps storage pendings = pendingOperations[caller];

        for (uint index = 0; index < pendings.opsCount; index++) {
            PendingOp storage pending = pendings.queue[index];
            if (pending.alreadySatisfied || tupleSpace[pending.template].multiplicity > 0) return int(index);
        }
        
        return -1;
    }
    
    /**
     * API method corresponding to `Linda`'s `out` primitive.
     * It retrieves a tuple matching `template` within the tuplespace.
     * - `template`: is the template a the tuple to be retrieved
     * - Cost: O(1) [if debug, O(tupleSpace[template].length)] 
     */
    function Read(string template) public {
        TupleInfo storage matching = tupleSpace[template];

        if (matching.multiplicity > 0) {
            pushPendingOperation(template, SuspensiveOps.READ, true, msg.sender);
            DebugSpaceUpdated(template, "read", msg.sender);
        } else {
            pushPendingOperation(template, SuspensiveOps.READ, false, msg.sender);
        }

        PendingOps storage pendings = pendingOperations[msg.sender];

        DebugQueueUpdated(template, pendings.queue.length - 1, pendings.queue.length, "read", msg.sender);
    }

    function GetResult() public returns (string tuple) {
        int i = getSatisfablePendingOperationForAddress(msg.sender);

        if (i < 0) {
            return "";
        }
        
        uint index = uint(i);

        PendingOps storage pendings = pendingOperations[msg.sender];
        PendingOp storage pending = pendings.queue[index];

        if (!pending.alreadySatisfied) {
            if (pending.operation == SuspensiveOps.TAKE) {
                tupleSpace[pending.template].multiplicity--;
                DebugSpaceUpdated(tuple, "take", msg.sender);
            } else {
                DebugSpaceUpdated(tuple, "take", msg.sender);
            }
        }
        
        tuple = pending.template;

        removeAtIndex(pendings.queue, index);
        pendings.opsCount--;

        DebugQueueUpdated(pending.template, pendings.opsCount + 1, pendings.opsCount, opToString(pending.operation), msg.sender);
    }

    function opToString(SuspensiveOps op) internal pure returns (string) {
        if (op == SuspensiveOps.TAKE) {
            return "take";
        } else if (op == SuspensiveOps.READ) {
            return "read";
        } else {
            return "ERROR";
        }
    }

    function removeAtIndex(PendingOp[] storage array, uint index) internal {
        uint i;
        for (i = index + 1; i < array.length; i++) {
            array[i - 1] = array[i];
        }
        array.length--;
    }
}