pragma solidity ^0.4.17;

/**
 * A Linda-like tuple space.
 */
contract TupleSpace {
    
    // An enumeration for suspensive operations, namely READ and TAKE.
    enum SuspensiveOperations {READ, TAKE}
    
    /**
     * A record storing some tuple's metadata.
     * - `quantity`: an integer indicating the amount of available copies of that tuple
     * - `queue`: a dynamically sized array storing informations about pending access requests on that tuple
     */
    struct TupleInfo {
        int quantity;
        WaitingPeer[] queue;
    }

    /**
     * A record storing some pending access request metadata.
     * - `peer`: is an address, i.e., an identifier of the waiting peer
     * - `operation`: keeps track of the pending access action nature: either READ or TAKE
     */
    struct WaitingPeer {
        address peer;
        SuspensiveOperations operation;
    }
    
    /**
     * Event notification template for tuple insertion.
     * - `tuple`: is the actually inserted tuple
     * - `provider`: is the address of the peer inserting it
     */
    event TupleWritten(string tuple, address provider);
    
    /**
     * Event notification template for tuple reading.
     * - `tuple`: is the actually read tuple
     * - `consumer`: is the address of the peer reading it
     */
    event TupleRead(string tuple, address consumer);

    /**
     * Event notification template for tuple retrieval.
     * - `tuple`: is the actually retrieved tuple
     * - `consumer`: is the address of the peer retrieve it
     */
    event TupleTaken(string tuple, address consumer);
    
    /**
     * Event notification template for any change to some tuple's queue of pending requests
     * This is not part of tuplespaces' interface but it useful for debugging purposes:
     * it may be triggered either when a new operation is scheduled or when it is executed.
     * - `tuple`: is the involved tuple
     * - `oldQueueLength`: is the length of the queue *before* the change occurs
     * - `newQueueLength`: is the length of the queue *after* the change occurred
     * - `operation`: represents the nature of the involved operation
     * - `peer`: is the address of the involved peer
     */
    event DebugQueueUpdated(string tuple, uint oldQueueLength, uint newQueueLength, SuspensiveOperations operation, address peer);
    
    /**
     * The smart contract constructor.
     * It simply stores its owner (= creator, in this case).
     * - `_name`: a string representing the name of the tuplespace,
     * - `_debug`: a boolean stating whether debug events should be generated or not
     */
    function TupleSpace(string _name, bool _debug) public payable {
        owner = msg.sender;
        debug = _debug;
        name = _name;
    }

    /**
     * The smart contract finalizer.
     * Simply refunds this smart contract balance (if any) to its owner
     * and let miners forget about its existence.
     */
    function Dispose() public {
        selfdestruct(owner);
    }
    
    // The address (i.e., the identifier) of this tuplespace's owner
    address owner;

    // A field stating whether debug events should be generated or not
    bool debug;

    // A publicly accessible field containing the name of the tuplespace for debugging purposes
    string public name;
    
    /**
     * This is the data structure actually reifying the tuplespace. 
     * It is a map associating each tuple contained within this tuplespace to its corresponding `TupleInfo` record.
     */
    mapping(string => TupleInfo) tupleSpace;
    
    /**
     * API method corresponding to `Linda`'s `out` primitive.
     * It inserts a tuple within the tuplespace.
     * - `tuple`: is the tuple to be inserted
     * - Cost: O(tupleSpace[tuple].length)
     */
    function Write(string tuple) public {
        // Let `info` be the `TupleInfo` record associated to `tuple` (defaults to the empty instance where `quantity` is `0` and `queue` is empty) 
        TupleInfo storage info = tupleSpace[tuple];
        // increases the quantity of available copies of `tuple` 
        info.quantity += 1;
        // generates the event `TupleWritten`: `msg.sender` is the address of the peer invoking the `write` method 
        TupleWritten(tuple, msg.sender);
        
        // for each request pending on `tuple` ...
        for (uint i = 0; i < info.queue.length; i++) {
            // ... serves it, and if `serveAny` returns `false` stops serving any further
            if (!serveAny(tuple)) break;
        }

        /*
        * Notice that the loop above is equivalent to
        * `while(!serveAny(tuple));`
        * but writing it as a (bounded for) improves Mist's gas estimation for this method
        */
    }
    
    /**
     * API method corresponding to `Linda`'s `in` primitive.
     * It retrieves a tuple matching `template` within the tuplespace.
     * - `template`: is the template a the tuple to be retrieved
     * - Cost: O(1) [if debug, O(tupleSpace[template].length)] 
     */
    function Take(string template) public {
        // Let `matching` be a tuple matching `template`
        TupleInfo storage matching = tupleSpace[template];
        // If at least one copy of `matching` is available...
        if (matching.quantity > 0) {
            // removes it
            matching.quantity -= 1;
            // generates the `TupleTaken` event
            TupleTaken(template, msg.sender);
        } else { // ... otherwise, if non is available
            // adds this take request to the list of pending requests for `template`
            scheduleTake(template); // O(1) [if debug, O(matching.queue.length)] 
        }
    }
    
    /**
     * API method corresponding to `Linda`'s `out` primitive.
     * It retrieves a tuple matching `template` within the tuplespace.
     * - `template`: is the template a the tuple to be retrieved
     * - Cost: O(1) [if debug, O(tupleSpace[template].length)] 
     */
    function Read(string template) public {
        // Let `matching` be a tuple matching `template`
        TupleInfo storage matching = tupleSpace[template];
        // If at least one copy of `matching` is available...
        if (matching.quantity > 0) {
            // generates the `TupleRead` event
            TupleRead(template, msg.sender);
        } else { // ... otherwise, if non is available
            // adds this take request to the list of pending requests for `template`
            scheduleRead(template); // O(1) [if debug, O(matching.queue.length)] 
        }
    }
    
    /**
    * Private method responsible for scheduling a TAKE operation
    * - `tamplate`: the template the TAKE operation is waiting for
    * - Cost: O(1) [if debug, O(queue.length)] 
    */
    function scheduleTake(string tamplate) internal {
        // Let `queue` be the list of pending requests for `tamplate` 
        WaitingPeer[] storage queue = tupleSpace[tamplate].queue;
        // Let `emptyIndex` be an empty index of `queue`
        uint emptyIndex = getEmptyIndex(queue); // O(1)
        // Fills that index with a new pending TAKE request: `msg.sender` is the address of the peer performing the request
        queue[emptyIndex] = WaitingPeer(msg.sender, SuspensiveOperations.TAKE);

        // If in debug mode...
        if (debug) {
            uint len = nonEmpties(queue); // O(queue.length)
            // Generates a `DebugQueueUpdated` notifying that the queue length for `tamplate` has increased because of a TAKE operation invoked by `msg.sender`
            DebugQueueUpdated(tamplate, len - 1, len, SuspensiveOperations.TAKE, msg.sender);
        }
    }
    
    /**
    * Private method responsible for scheduling a READ operation
    * - `tamplate`: the template the READ operation is waiting for
    * - Cost: O(1) [if debug, O(queue.length)] 
    */
    function scheduleRead(string tamplate) internal {
        // Let `queue` be the list of pending requests for `tamplate` 
        WaitingPeer[] storage queue = tupleSpace[tamplate].queue;
        // Let `emptyIndex` be an empty index of `queue`
        uint emptyPos = getEmptyIndex(queue); // O(1)
        // Fills that index with a new pending READ request: `msg.sender` is the address of the peer performing the request
        queue[emptyPos] = WaitingPeer(msg.sender, SuspensiveOperations.READ);

        // If in debug mode...
        if (debug) {
            uint len = nonEmpties(queue); // O(queue.length)
            // Generates a `DebugQueueUpdated` notifying that the queue length for `tamplate` has increased because of a READ operation invoked by `msg.sender`
            DebugQueueUpdated(tamplate, len - 1, len, SuspensiveOperations.READ, msg.sender);
        }
    }
    
    /**
    * Private method responsible for serving a pending operation.
    * Serving a READ operation means generating the corresponding event.
    * Similarly, serving a TAKE operation means removing the tuple and generating the corresponding event.
    * - `tuple`: the tuple which may be used to serve a 
    * - Cost: O(tuple.queue.length)
    */
    function serveAny(string tuple) internal returns (bool) {
        // Let `info` be the the metadata relative to `tuple`
        TupleInfo storage info = tupleSpace[tuple];
        // If the quantity of copies of `tuple` is 0 returns false: no READ or TAKE can be served
        if (info.quantity < 1) {
            return false;
        }

        // At least a copy of `tuple` exists, then let `queue` be `tuple`'s pending operations list
        WaitingPeer[] storage queue = info.queue;

        // Selects an operation in `queue`
        uint selectedIndex = selectIndex(queue); // O(queue.length)
        // If the index if out of range, there is no pending request then returns false
        if (selectedIndex >= queue.length) {
            return false;
        }

        // A pending operation was selected, then let `selected` be such a selected operation
        WaitingPeer storage selected = queue[selectedIndex];

        // If `selected` is a TAKE operation ...
        if (selected.operation == SuspensiveOperations.TAKE) {
            // ... generate a `TupleTaken` event
            TupleTaken(tuple, selected.peer);
            // ... remove a copy of `tuple`
            info.quantity--;
        } else if (selected.operation == SuspensiveOperations.READ) { // If `selected` is a READ operation ...
            // ... generate a `TupleRead` event
            TupleRead(tuple, selected.peer);
        } else { // Any other case is a bug...
            // then aborts the whole transaction reverting side effects
            revert();
        }

        // If in debug mode...
        if (debug) {
            uint len = nonEmpties(queue); // O(1)
            // Generates a `DebugQueueUpdated` notifying that the queue length for `tuple` decreased in length because of a pending operation has been served
            DebugQueueUpdated(tuple, len + 1, len, selected.operation, selected.peer);
        }
        // Removes the selected request from the pending list
        removeAtIndex(queue, selectedIndex);
        // Returns true, meaning that a request was served
        return true;
    }

    /**
     * Private method aimed at selecting the index of one pending request within some requests array.
     * - `array`: is an array of pending requests
     * - returns: an index of the aforementioned array, if it contains at least one pending request, or its length, otherwise
     * - Cost: O(array.length) 
     * 
     * This is deliberately more complex than a simple iteration from index 0 to the last one.
     * The purpose of this strategy is to provide some variability to the requests serving order.
     */
    function selectIndex(WaitingPeer[] storage array) internal view returns(uint) {
        uint i;
        for (i = array.length / 2; i >= 0; i--) {
            if (array[i].peer != 0) return i;
        }
        for (i = array.length / 2 + 1; i < array.length; i++) {
            if (array[i].peer != 0) return i;
        }
        return array.length;
    }

    /**
     * - Counts the amount of non-empty requests within an array of pending requests 
     * - `array`: is an array of pending requests
     * - returns: the actual amount of pending requests contained within the array
     * - Cost: O(array.length) 
     */
    function nonEmpties(WaitingPeer[] storage array) internal view returns(uint) {
        uint len = 0;
        for (uint i = 0; i < array.length; i++) {
            if (array[i].peer != 0) len++;
        }
        return len;
    }
    
    /**
     * Gets the index of an empty position within the provided array of pending requests
     * - `array`: is an array of pending requests
     * - returns: the index of an array item containing no actual pending request
     * - Cost: O(1) [see https://stackoverflow.com/questions/33839122/in-ethereum-solidity-when-changing-an-array-length-i-get-value-must-be-an-lva]
     */
    function getEmptyIndex(WaitingPeer[] storage array) internal returns(uint) {
        uint result = array.length;
        array.length++;
        return result;
    }

    /**
     * Actually removes an item having a given index from an array of pending requests, shrinking the array accordingly
     * - `array`: is an array of pending requests
     * - Cost: O(array.length) 
     */
    function removeAtIndex(WaitingPeer[] storage array, uint index) internal {
        uint i;
        for (i = index + 1; i < array.length; i++) {
            array[i - 1] = array[i];
        }
        delete array[i - 1];
        array.length--;
    }
}