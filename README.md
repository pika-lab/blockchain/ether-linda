# Blockchain for Fully Decentralised Coordination: A First Study with Linda and Ethereum
Giovanni Ciatto, Stefano Mariani, Andrea Omicini

## Useful links
- [__Link to the commented Solidity source code implementing `Linda`'s tuple spaces__](https://gitlab.com/das-lab/blockchain/ether-linda/snippets/1701407)
- [Link to the Solidity documentation](https://solidity.readthedocs.io)

## Tuple spaces as smart contracts

We propose a conceptual mapping between tuple spaces and Ethereum smart contracts.
Smart contracts are essentially objects (in the OOP sense) encapsulating state and methods.
You can read a smart contract code similarly to how yoy read a class code in any objecte-oriented, class-based language.

According to our work, a tuple space is a smart contract providing the following public interface:

```solidity 
contract Linda {

    function write(string tuple) public { ... }
    event TupleWritten(string tuple, address provider);

    function read(string template) public { ... }
    event TupleRead(string tuple, address consumer);
    
    function take(string template) public { ... } 
    event TupleTaken(string tuple, address consumer);
    
}
```

A client (identified by its `address`) can *initiate* a `write` operation, aimed at inserting the tuple `"foo"` into the tuplespace, by invoking the method (i.e., by sending a message having the form) `write("foo")` on a `Linda` contract instance.
The *completion* of such an operation can be recognized by the client by inspecting the blockchain searching for a notification of the `TupleWritten` event.
Such an event notification carries two informations:
- a representation of the tuple being inserted
- the `address` of the client inserting it 

Clients may check for the existence of a tuple by means of the `read` operation or remove it by means of the `take` one.
In both cases the operation requires a tuple template to be provided in the form of string.
For the sake of simpliciy, the matching mechanism employed by the current implementation is essentially string comparison, thus the template must be *exactly equal* to the matched tuple.
We plan to extend the mathching mechanism as a future work.

A client can *initiate* a `read` operation, aimed at waiting for a tuple matching `"foo"` to be available within the tuplespace, by invoking the method (i.e., by sending a message having the form) `read("foo")` on a `Linda` contract instance.
The *completion* of such an operation can be recognized by the client by inspecting the blockchain searching for a notification of the `TupleRead` event.
Such an event notification carries two informations:
- a representation of the tuple being read
  + in future implementations the specific tuple to be retrieved may actually be different from its template, this is the real reason behind this modeling of the `TupleRead` event
- the `address` of the client requesting it

A client can *initiate* a `take` operation, aimed at retrieving a tuple matching `"foo"` from the tuplespace, by invoking the method (i.e., by sending a message having the form) `take("foo")` on a `Linda` contract instance.
The *completion* of such an operation can be recognized by the client by inspecting the blockchain searching for a notification of the `TupleWritten` event.
The `take` operation completion implies that the retrieved tuple has been removed from the tuplespace.
Such an event notification carries two informations:
- a representation of the tuple being read
- the `address` of the client retrieving it 

## Implementation details
 
The Solidity source code is provided [here](https://gitlab.com/das-lab/blockchain/ether-linda/snippets/1701407).
A line-by-line description of the code is provided too.

## How to setup the Ethereum development enviroment

The macro steps to test our Etherum base Linda implementation are the following:
- Download and install Geth and Mist
- Deploy a local test blockchain by running Geth in `dev` mode
- Run Mist and deploy the provided solidity code
- Enjoy your tuplespace :)

### Download & Install Geth and Mist and deploy an Ethereum solo-blockchain

- Download __Geth__ for your system from https://geth.ethereum.org/downloads/
    + Consider downloading the archive, not the installer
    + We suppose the download folder is `path/`

- Download __Mist__ from https://github.com/ethereum/mist/releases
    + Consider downloading the archive, not the installer
    + We suppose the download folder is `path/`

- Emulate the steps shown in this video: https://youtu.be/F4WZh5tyFKM, that are:
    0. Unpack the Geth client in `path/geth-<version>/`
    0. Open a shell and run the command `path/geth-<version>/geth --datadir path/geth-<version>/eth --dev --dev.period 1`
        * This will make Geth run a local blochchain where a block is published once every 1 second
        * A password-free user will be created too, owning a very high amount of fake ETH
    0. Unpack the Mist wallet in `path/<version>-unpacked/`
    0. Open another shell and run the command `path/<version>-unpacked/Mist` then way for a window to appear
    0. Get confident with the Mist wallet and don't worry about security alerts: you aren't owning any real ETH and you aren't connected to the real Ethereum blockchain

### Deploy a TupleSpace smart contract

Emulate the steps shown in this video: https://youtu.be/5tI2qqcskXQ, that are:
0. Open the `Contracts` page and click on the `Deploy new contract`
0. A page will be opened where you can copy&paste our solidity code of [a TupleSpace smart contract](https://gitlab.com/das-lab/blockchain/ether-linda/snippets/1701407)
0. After a brief compilation process, you will be allowed to choose the contract to be doployed on the right: 
    - choose `Tuple Space`
    - provide a name for the tuplespace
    - set the debug mode to true
0. Click on `Deploy` on the bottom of the page
0. A popup will appear asking for your password: no password is required then you can simply click on `Send Transaction`
0. Your account page will appear and, on the bottom, you will be able to see a progress bar showing the confirmation process of your contract deployment
0. Wait for 12 confirmations (the actual time to await depends on the `X` you set in `--dev.period X` when launching Geth)
0. Go back to the `Contracts` page and click on the newly deployed contract
0. A page will be opened from which you can send transactions to the `Tuple Space` contract

### Testing the TupleSpace smart contract

You can invoke `Tuple Space`'s methods from its page, which you should have reached following the instruction so far.
To do so, try emulating the steps shown in this video: https://youtu.be/U2ckB48yh7I, that are:
0. Start listening for events by activating the `Watch for events` combo box:
0. Select/pick a function from the right of the page and provide arguments to the selcted function
0. To execute it, click on `Execute` and then authorize the transaction.
0. If the function execution generates some side effects, you will see then on the bottom.
0. Enjoy :)
